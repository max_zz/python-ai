# Python-AI

#### 介绍
Python与人工智能实践 （鲁东大学信电学院人工智能教研室）
讲解视频已经上传到B站 可以搜索关注 bugyu_ld


#### 算法篇

1.  KNN (K近邻分类器)  
    讲解视频：https://www.bilibili.com/video/BV1HX4y137TN/

2.  DecisionTree(决策树分类器)  
    讲解视频：https://www.bilibili.com/video/BV1mK4y1U79E

3.  NaiveBayes(朴素贝叶斯分类器)  
    讲解视频：https://www.bilibili.com/video/BV1oX4y137p9

4.  Kmeans(K均值聚类算法)  
5.  GMM(高斯混合模型)  
    Kmeans+GMM讲解视频：https://www.bilibili.com/video/BV1oy4y147L8/

6.  DensityPeak(密度峰值聚类算法)  
    讲解视频：https://www.bilibili.com/video/BV1Yy4y1h7gG

7.  logisticRegression(逻辑回归分类算法)  
    讲解视频：https://www.bilibili.com/video/BV1pK411w7ke/

8.  BP-NN(BP神经网络)  
    讲解视频：https://www.bilibili.com/video/BV1iy4y1t7nx  
    
    增加relu激活函数  
    讲解视频：https://www.bilibili.com/video/BV1Pb4y1C7YF/  

    
    mnist数据下载地址：  链接：https://pan.baidu.com/s/1LBjlGiphP5mx7h3EfuCO7A   
        提取码：pqzl 


9.  算法篇小节与Numpy简介  
    讲解视频：https://www.bilibili.com/video/BV1KZ4y1c7Eb 

10. HMM(隐马尔科夫模型)  
    (1) HMM简介与前向后向算法  
    视频讲解：https://www.bilibili.com/video/BV15q4y1j7oH/  
    (2) 维特比译码 (Viterbi)  
    视频讲解：https://www.bilibili.com/video/BV1Yh411e7cS/    
    (3) HMM 模型训练(鲍姆-韦尔奇算法 Baum-Welch )  
    视频讲解：https://www.bilibili.com/video/BV1M54y157TS/    
    (4) GMM-HMM  
    视频讲解：https://www.bilibili.com/video/BV1o64y1R71e/  
    (5) hmmlearn的使用  
    视频讲解：https://www.bilibili.com/video/BV1Hh411Y7rn/  
11. 基于GMM-HMM的孤立词识别（GMMHMM-Speech）  
    讲解视频1：利用hmmlearn实现  
    https://www.bilibili.com/video/BV1KV411W7YQ/  
    讲解视频2：利用修改的GMM-HMM模型实现  
    https://www.bilibili.com/video/BV1ky4y1M7rM/   
12. DBSCAN 聚类算法  
    讲解视频：https://www.bilibili.com/video/BV1vL41147Ah/    
13. 数据降维 (dimension-reduce)  
    (1) PCA 降维    
    视频讲解： https://www.bilibili.com/video/BV1n34y1D7xV/    
    (2) MDS 降维    
    视频讲解： https://www.bilibili.com/video/BV11U4y1A71u/  
    (3) Kernel PCA 降维  
    视频讲解： https://www.bilibili.com/video/BV1xu411f7xp/  
    (4) ISOMAP 算法  
    视频讲解：https://www.bilibili.com/video/BV1hL4y1z7Lw/    
    (5) LLE 算法  
    视频讲解：https://www.bilibili.com/video/BV14g411F7Je/  
    (6) T-SNE算法  
    视频讲解：https://www.bilibili.com/video/BV1cU4y1w74A/  
    mnist 数据下载:  
    链接：https://pan.baidu.com/s/1I0nCDC7JvKkLhB4IIAHYVg   
    提取码：x2pu  




#### 应用篇

1.  OpenCV简介(给小猫加眼镜)  
    视频讲解：https://www.bilibili.com/video/BV1zK4y1K7wt/  

    字库文件下载地址：  
    链接：https://pan.baidu.com/s/1el4uiqXK4nSsKfYGnif1lw  
    提取码：ovd3   

2.  Scanner 基于tesseract的文件扫描仪  
    讲解视频 : https://www.bilibili.com/video/BV1cB4y1A7yS/  
    tesseract 安装文件以及简体中文识别模型下载地址：  
    链接：https://pan.baidu.com/s/1hkTqLjDcQ6NEaf0bV27VsQ   
    提取码：xjos  

3.  facedetect 基于Dlib的人脸及关键点检测+简单特效  
    讲解视频：  
    https://www.bilibili.com/video/BV1hb4y1D7kt   
    关键点检测模型下载：  
    链接：https://pan.baidu.com/s/1om55sY4XAgd24ww6Ds-4WA   
    提取码：6emc   
   

4. face_recongize  基于Dlib的人脸识别  
    讲解视频： https://www.bilibili.com/video/BV1mK4y1P7kP/  
    模型下载：  
    链接：https://pan.baidu.com/s/1qkluZ6fffk8gnlnN7LUJ6Q   
    提取码：4vtf  


5. image classification  基于tflite的图像分类     
    讲解视频： https://www.bilibili.com/video/BV1KQ4y1o7Vc/  

6. object detection  基于tflite的目标检测  
    讲解视频：  https://www.bilibili.com/video/BV1To4y1m7c7/ 

7. Posnet 基于POSnet的目标检测与动作识别  
视频讲解：https://www.bilibili.com/video/BV1S5411u7BB/   
模型下载：https://pan.baidu.com/s/1cCLUPqNvQc7-iiYc7AaBjw   
提取码：34c0   

8. StyleTransform 图像风格转换  
视频讲解：https://www.bilibili.com/video/BV1fK4y1G7Fh/  
模型下载: 模型下载：链接：https://pan.baidu.com/s/1vFureC3UIPS226GqfP9HgA    
提取码：asgh  


